data "azurerm_resource_group" "rg-terraform2" {
  name = "rg-terraform"
}

resource "azurerm_virtual_network" "network" {
  name                = "vnet"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.rg-terraform2.location
  resource_group_name = data.azurerm_resource_group.rg-terraform2.name
}

resource "azurerm_subnet" "sbterraform" {
  name                 = "subnet-default-changed"
  resource_group_name  = data.azurerm_resource_group.rg-terraform2.name
  virtual_network_name = azurerm_virtual_network.network.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_subnet" "sbterraform-2" {
  name                 = "second-subnet"
  resource_group_name  = data.azurerm_resource_group.rg-terraform2.name
  virtual_network_name = azurerm_virtual_network.network.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_security_group" "firstGroup" {
  name                = "securityGroup01"
  location            = data.azurerm_resource_group.rg-terraform2.location
  resource_group_name = data.azurerm_resource_group.rg-terraform2.name

  security_rule {
    name                       = "test1"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "test"
  }
}


