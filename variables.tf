variable "resource_group" {
  type = string
  default = "rg-terraform"
}

variable "storage_account" {
  type = string
  default = "act223085"
}

variable "container" {
  type = string
  default = "c-terraform"
}