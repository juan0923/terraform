
#terraform.tf
terraform {
  backend "azurerm" {
    resource_group_name  = "rg-terraform"
    storage_account_name = "act223085"
    container_name       = "c-terraform"
    key                  = "terraform.tfstate"
  }
  required_providers {
    azurerm = "~> 2.0"
  }
}